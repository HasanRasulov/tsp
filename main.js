let canvas_x = 600,
    canvas_y = 600;
class City {

    constructor(x, y, id) {
        this.x = x;
        this.y = y;
        this.id = id;
    }

    get_distance(city) {

        let x = Math.abs(this.x - city.x);
        let y = Math.abs(this.y - city.y);

        return Math.sqrt(x * x + y * y);
    }

};

class Tour {

    constructor(cities) {
        this.tour = cities;
        this.fittness = 0.0;
        this.distance = 0.0;

        this.get_distance_for_tour();
        this.get_fitness();
    }



    get_distance_for_tour() {

        if (this.distance != 0.0) return this.distance;

        let d = 0.0;

        for (let i = 0; i < this.tour.length; i++) {

            let from = this.tour[i];
            let to = null;

            if (i + 1 < this.tour.length) {
                to = this.tour[i + 1];
            } else {
                to = this.tour[0];
            }
            d += from.get_distance(to);
        }


        return (this.distance = d);

    }

    get_fitness() {
        return this.fittness != 0 ? this.fittness : this.fittness = (1 / this.distance);
    }


    is_contain(city) {

        for (let i = 0; i < this.num_of_cities; i++) {
            if (this.tour[i].id === city.id) return true;
        }

        return false;
    }

}

class Population {



    constructor(pop_sz, num_of_cities, mut_rate) {

        this.population = [];
        this.population_size = pop_sz;
        this.number_of_cities = num_of_cities;
        this.mutation_rate = mut_rate;

        let tour = [];

        for (let i = 0; i < this.number_of_cities; i++) {
            let c = new City(random(0, canvas_x), random(0, canvas_y), i);
            tour.push(c);
        }


        let t = new Tour(tour);
        this.population.push(t);

        for (let i = 1; i < this.population_size; i++) {
            tour = shuffle(tour);
            this.population.push(new Tour(tour));
        }
    }

    selection() {

         console.log("--------------Selection--------------");
        let parents = [];

        let mother = this.get_fittest_individual();
        parents.push(mother);

        let father = this.population[0];

        for (let i = 1; i < this.population.length; i++) {
            if (father != mother && father.fitness <= this.population[i].fittness) {
                father = this.population[i];
            }
        }

        parents.push(father);

        return parents;
    }

    crossover(parents) {

                console.log("--------------Crossover--------------");

        let mother = parents[0],
            father = parents[1];
        let child_tour = new Array(this.number_of_cities);
        for (let i = 0; i < this.number_of_cities; i++) {
            child_tour[i] = new City(-1, -1, -1);
        }
        let child = new Tour(child_tour);

        let cros = floor(random(0, this.number_of_cities));
        let num = floor(random(0, this.number_of_cities - cros));


        for (let i = cros; i < num + cros; i++) {
            child.tour[i].id = mother.tour[i].id;
            child.tour[i].x = mother.tour[i].x;
            child.tour[i].y = mother.tour[i].y;
        }


        let j = 0;
        for (let i = 0; i < this.number_of_cities; i++) {

            while (j >= cros && j < cros + num) j++;

            if (!child.is_contain(father.tour[i])) {
                child[j++] = father.tour[i];
            }

        }

        return child;
    }


    swap(tour, i, j) {
        let tmp = tour[i];
        tour[i] = tour[j];
        tour[j] = tmp;
    }

    mutation(tour) {

                console.log("--------------Mutation--------------");

        for (let i = 0; i < this.number_of_cities; i++) {

            if (random(0, 1) <= this.mutation_rate) {

                let x = floor(random(0, this.number_of_cities));
                let y = floor(random(0, this.number_of_cities));

                this.swap(tour.tour, x, y);

            }

        }

    }

    get_fittest_individual() {

        let t = this.population[0];

        for (let i = 1; i < this.population.length; i++) {
            if (t.fittness <= this.population[i].fittness) {
                t = this.population[i];
            }
        }
        return t;
    }


    add(tour) {
        this.population.push(tour);
        this.population_size++;
    }


    print_individual(ind) {
        console.log("----------Individual------------" + "  Distance:" + ind.distance);
        for (let i = 0; i < ind.tour.length; i++) {
            console.log(ind.tour[i]);
        }
        console.log("\n");
    }

    print_population() {
        for (let i = 0; i < this.population_size; i++) {
            console.log("Tour-" + i);
            this.print_individual(this.population[i]);
        }
    }

    print_mating_pool(parents) {

        console.log("-------Mother-------");
        this.print_individual(parents[0]);

        console.log("-------Father-------");
        this.print_individual(parents[1]);

    }

}


function setup(){

let p = new Population(10, 5, 0.0);

let ind_for_initial = p.get_fittest_individual();

console.log("Fittest Individual from initial generation:");

p.print_individual(ind_for_initial);


let num = 1;
while (num--) {
   // p.print_population();        
    let parents = p.selection();
    p.print_mating_pool(parents);
    let child = p.crossover(parents);
    p.print_individual(child); 
    p.mutation(child);
    p.add(child);
}



let ind_for_last_gen = p.get_fittest_individual();

console.log("Fittest Individual from last generation:");

p.print_individual(ind_for_last_gen);

}
function draw(){}

